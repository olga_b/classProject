package utils;

import core.common.AbstractPage;
import core.common.annotations.PagePartialPath;

import java.util.Optional;

/**
 * Created by Hillel4 on 11.12.2017.
 */
public class WebDriverUtil {

    private WebDriverUtil(){

    }

    public static <T extends AbstractPage> String getFullNavigationUrl(final Class <T> page,final String baseUrl){
        final PagePartialPath partialPath = page.getAnnotation(PagePartialPath.class);
        return Optional.ofNullable(partialPath)
                .map(PagePartialPath::value)
                .map(item -> String.format("%s%s",baseUrl,item))
                .orElse(null);

        //final PagePartialPath pagePartialPath= page.getAnnotation(PagePartialPath.class);
        //return String.format("%s%s", baseUrl, pagePartialPath.value());
    }

    /*public static void main(String[] args) {
        String url = getFullNavigationUrl(AlcoholicDrinksCategoryPage.class, "https://rozetka.com.ua");
    }*/
}
