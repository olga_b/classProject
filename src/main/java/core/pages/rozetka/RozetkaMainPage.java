package core.pages.rozetka;

import core.common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Hillel4 on 04.12.2017.
 */
public class RozetkaMainPage extends AbstractPage{

    @FindBy(xpath = "//input[contains(@class,'rz-header-search-input-text passive')]")
    private WebElement searchInputField;

    @FindBy(xpath = "//button[@type='submit' and contains(@class, 'btn-link-i js-rz-search-button')]")
    private WebElement findButton;


    public RozetkaMainPage(final WebDriver webDriver){
        super(webDriver);
    }

    public SearchResultPage searchByText(final String text){
        searchInputField.sendKeys(text);
        findButton.click();
        return new SearchResultPage(getDriver());
    }



}
