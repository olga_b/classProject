package core.pages.rozetka;

import core.common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Hillel4 on 07.12.2017.
 */
public class StrongAlcoholPage extends AbstractPage {

    @FindBy(xpath = "//a[@href='https://rozetka.com.ua/krepkie-napitki/c4594292/']")
    private WebElement link;

    public StrongAlcoholPage(WebDriver webDriver) {
        super(webDriver);
    }

    public StrongAlcoholPage openAlcohol(){
        link.click();

        return new StrongAlcoholPage(getDriver());
    }
}
