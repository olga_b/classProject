package core.pages.rozetka;

import core.common.AbstractPage;
import core.common.annotations.PagePartialPath;
import org.openqa.selenium.WebDriver;

/**
 * Created by Hillel4 on 11.12.2017.
 */
@PagePartialPath(value = "/alkoholnie-napitki-i-produkty/c4626923/")
public class AlcoholicDrinksCategoryPage extends AbstractPage {

    protected AlcoholicDrinksCategoryPage(WebDriver webDriver){
        super(webDriver);
    }
}
