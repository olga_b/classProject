package core.pages.rozetka;

import core.common.AbstractPage;
import core.common.annotations.Convertable;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Hillel4 on 04.12.2017.
 */
@Convertable
public class SearchResultPage extends AbstractPage{

    @FindBy(xpath = "//span[@id='search_result_title_text']")
    private WebElement searchResultLabel;

    protected SearchResultPage(final WebDriver webDriver){
        super(webDriver);
    }

    public String getSearchResultLabelText(){

        return searchResultLabel.getText();
    }
}
