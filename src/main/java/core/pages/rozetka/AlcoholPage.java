package core.pages.rozetka;

import core.common.AbstractPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Hillel4 on 07.12.2017.
 */
public class AlcoholPage extends AbstractPage {

    public AlcoholPage(WebDriver webDriver) {
        super(webDriver);
    }

    @FindBy(xpath = "//a[@data-title='Алкогольные напитки и продукты']")
    private WebElement alcoholLink;

    public AlcoholPage openAlcohol(){
        alcoholLink.click();

        return new AlcoholPage(getDriver());
    }

    public StrongAlcoholPage openNext(){
        StrongAlcoholPage page2 = new StrongAlcoholPage(getDriver());
        page2.openAlcohol();
        return new StrongAlcoholPage(getDriver());
    }

}
