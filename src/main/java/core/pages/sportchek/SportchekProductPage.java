package core.pages.sportchek;

import core.common.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by olya on 13.12.17.
 */
public class SportchekProductPage extends AbstractPage {

    public SportchekProductPage(WebDriver webDriver) {
        super(webDriver);
    }
    @FindBy(xpath = "//select[@data-control-type = 'size']")
    private WebElement sizeControl;

    @FindBy(xpath = "//select[@name = 'quantity']")
    private WebElement qtyControl;

    @FindBy(xpath = "//button[contains(@class, 'add-cart')]")
    private WebElement addToCart;

    @FindBy(xpath = "//a[@class = 'header-cart__trigger drawer-ui__toggle']")
    private WebElement shoppingCart;

    public ShoppingCartPage chooseAndPutProductToCart() throws InterruptedException {

        waitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@data-control-type = 'size']")));
        waitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath("//select[@name = 'quantity']")));

        Select selectSize = new Select(sizeControl);
        selectSize.selectByVisibleText("M/L");

        Select selectQty = new Select(qtyControl);
        selectQty.selectByValue("1");

        Thread.sleep(500);

        addToCart.click();

        Thread.sleep(500);

        shoppingCart.click();

        return new ShoppingCartPage(getDriver());

    }


}
