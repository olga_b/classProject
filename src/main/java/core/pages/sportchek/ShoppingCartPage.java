package core.pages.sportchek;

import core.common.AbstractPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by olya on 13.12.17.
 */
public class ShoppingCartPage extends AbstractPage {

    @FindBy(xpath = "//div[@data-module-type='ProductList']")
    private WebElement productList;

    @FindBy(xpath = "//input[@class='sc-product__qty']")
    private WebElement productQty;

    @FindBy(xpath = "//a[@class = 'sc-product__update']")
    private WebElement update;

    @FindBy(xpath = "//span[@class = 'sc-product__price']")
    private WebElement price;

    @FindBy(xpath = "//span[text()='Estimated Total:' and @class = 'co-folder__line-item__title']")
    public WebElement estimatedTotal;

    public ShoppingCartPage(WebDriver webDriver) {
        super(webDriver);
    }

    public boolean isShoppingCartEmpty(){

        return productList == null ? true : false;
    }

    public void updateQuantity(int qty) throws InterruptedException {

        productQty.clear();
        productQty.sendKeys(String.valueOf(qty));
        try {
            if (update.isDisplayed()) {
                waitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class = 'sc-product__update']")));
                update.click();
            }
        } catch(NoSuchElementException e){

        }


    }

    public Double getShoppingCartPrice() {

        return Double.parseDouble(price.getText());
    }




}
