package core.pages.sportchek;

import core.common.AbstractPage;
import core.widgets.sportchek.HeaderWidget;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Hillel4 on 14.12.2017.
 */
public class SportchekMainPage extends AbstractPage {

    @FindBy(xpath = "//header[@class='page-header']")
    private HeaderWidget header;

    public SportchekMainPage(WebDriver webDriver) {
        super(webDriver);
    }
}
