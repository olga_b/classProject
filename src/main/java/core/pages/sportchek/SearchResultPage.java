package core.pages.sportchek;

import core.common.AbstractPage;
import core.widgets.sportchek.FilterWidget;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Hillel4 on 14.12.2017.
 */
public class SearchResultPage extends AbstractPage {

    @FindBy(xpath = "//div[@class='facets-side']")
    private FilterWidget filterWidget;

    public SearchResultPage(final WebDriver webDriver) {
        super(webDriver);
    }
}
