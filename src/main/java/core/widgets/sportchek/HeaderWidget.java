package core.widgets.sportchek;

import core.common.AbstractWidget;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Hillel4 on 14.12.2017.
 */
public class HeaderWidget extends AbstractWidget {

    @FindBy(xpath = "//input[@class='header-search__input ui-autocomplete-input rfk_sb']")
    private WebElement searchBar;

    @FindBy(xpath = "//input[@class='header-search__submit button']")
    private WebElement searchButton;

}
