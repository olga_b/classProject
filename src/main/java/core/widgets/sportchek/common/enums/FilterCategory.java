package core.widgets.sportchek.common.enums;

/**
 * Created by Hillel4 on 14.12.2017.
 */
public enum FilterCategory {

    GENDER("Gender"),
    CATEGORY("Category"),
    BRANDS("Brands"),
    SIZE("Size"),
    COLOUR("Colour"),
    AVAILABLE("Available"),
    PRICE("Price");

    private String item;

    FilterCategory(final String item) {
        this.item = item;
    }

    public String getName(){
        return this.item;
    }
}
