package core.common;

import jdk.nashorn.internal.objects.annotations.Function;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Hillel4 on 04.12.2017.
 */
public abstract class AbstractPage {

    private WebDriver driver;
    private WebDriverWait wait;

    protected AbstractPage(final WebDriver webDriver){

        this.driver = webDriver;
        PageFactory.initElements(webDriver, this);
    }

    protected WebDriver getDriver(){
        return driver;
    }

    public void waitUntil(final ExpectedCondition condition){
        wait = new WebDriverWait(driver, 10);
        wait.until(condition);

    }

    public void convert(final List<WebElement> elements){

        List<String> listElements = elements.stream()
                .map(item -> item.getText())
                .collect(Collectors.toList());
    }



}
