package rozetka;

import common.BaseTest;
import core.pages.rozetka.RozetkaMainPage;
import core.pages.rozetka.SearchResultPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

/**
 * Created by Hillel4 on 30.11.2017.
 */
public class RozetkaTest extends BaseTest {

    private WebDriver driver = super.getDriver();
    private final String expectedResultText = "sony playstation 4";

    public RozetkaTest(){
        super();
    }

    @Before
    public void initDriver() {
        driver.get("https://rozetka.com.ua");
    }

    @Test
    public void rozetkaSearchFunctionalityTest() throws InterruptedException {

        final RozetkaMainPage mainPage = new RozetkaMainPage(driver);
        Thread.sleep(500);
        final SearchResultPage resultPage = mainPage.searchByText(expectedResultText);
        Assert.assertEquals("There is incorrect text displayed!",expectedResultText,resultPage.getSearchResultLabelText());

        /*final AlcoholPage alcoholPage = new AlcoholPage(driver);
        alcoholPage.openAlcohol();
        final StrongAlcoholPage strongAlcoholPage = alcoholPage.openNext();*/

    }


    @After
    public void tearDown(){
        driver.close();
        driver.quit();
    }

}
