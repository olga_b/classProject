package common;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * Created by Hillel4 on 07.12.2017.
 */
public abstract class BaseTest {

    private WebDriver driver;

    protected BaseTest(){
        initDriver();
    }

    protected WebDriver getDriver(){
        return driver;
    }

    private void initDriver(){
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        //wait = new WebDriverWait(driver,10);
    }
}
