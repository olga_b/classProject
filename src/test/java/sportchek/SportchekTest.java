package sportchek;

import common.BaseTest;
import core.pages.sportchek.ShoppingCartPage;
import core.pages.sportchek.SportchekProductPage;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

/**
 * Created by olya on 13.12.17.
 */
public class SportchekTest extends BaseTest {

    private WebDriver driver = super.getDriver();
    private int qty = 2;

    public SportchekTest(){
        super();
    }

    @Test
    public void chooseBackpack() throws InterruptedException {
        driver.get("https://www.sportchek.ca/product/332237753.html#332237753=332237756");
        SportchekProductPage productPage = new SportchekProductPage(driver);
        productPage.chooseAndPutProductToCart();

        ShoppingCartPage shoppingCartPage = new ShoppingCartPage(driver);
        boolean isEmpty = shoppingCartPage.isShoppingCartEmpty();

        Assert.assertFalse("Shopping cart is empty", isEmpty);

        shoppingCartPage.updateQuantity(2);
        Double price = shoppingCartPage.getShoppingCartPrice();

        shoppingCartPage.waitUntil(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[text()='Estimated Total:' and @class = 'co-folder__line-item__title']")));

        Assert.assertEquals("Estimated total hasn't changed",shoppingCartPage.estimatedTotal,qty*price);
        //shoppingCartPage.waitUntil(5, TimeUnit.SECONDS);

        Thread.sleep(500);
    }

    @After
    public void tearDown(){
        driver.close();
        driver.quit();
    }
}
